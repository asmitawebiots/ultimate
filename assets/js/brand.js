/*************************
 brand js start
 *************************/

$('#app2-brand').owlCarousel({
    items: 6,
    margin: 80,
    autoHeight: true,
    nav: false,
    dots:false,
    autoplay: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    loop: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 2,
            margin: 10
        },
        360: {
            items: 3,
            margin: 20
        },
        480: {
            items: 4,
            margin: 30
        },
        576: {
            items: 5,
            margin: 30
        },
        767: {
            items: 5,
            margin: 20
        },
        991: {
            items: 6
        },
        1000: {
            items: 6
        }
    }
});

/*************************
 brand js end
 *************************/
/*************************
 gym-brand js start
 *************************/

$('#gym-brand').owlCarousel({
    items: 6,
    margin: 50,
    autoHeight: true,
    nav: false,
    dots: false,
    autoplay: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    loop: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 1,
            margin: 10
        },
        480:{
            items: 2,
            margin: 30
        },
        576:{
            items: 3,
            margin: 30
        },
        767:{
            items: 4,
            margin: 30
        },
        991: {
            items: 4,
            margin: 30
        },
        1000: {
            items: 5
        },
        1600: {
            items: 6
        }
    }
});

/*************************
 gym brand js end
 *************************/
/*************************
 saas-brand js start
 *************************/

$('#saas1-brand').owlCarousel({
    items: 8,
    margin: 80,
    autoHeight: true,
    nav: false,
    dots: false,
    autoplay: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    loop: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 1,
            margin: 10
        },
        480:{
            items: 2
        },
        576:{
            items: 3
        },
        767:{
            items: 4,
            margin: 40
        },
        991: {
            items: 5,
            margin: 30
        },
        1000: {
            items: 8
        }
    }
});

/*************************
 saas-brand js end
 *************************/


/*************************
 brand js start
 *************************/

$('#saas2-brand').owlCarousel({
    items: 5,
    margin: 80,
    autoHeight: true,
    nav: false,
    dots:false,
    autoplay: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    loop: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 1,
            margin: 10
        },
        360: {
            items: 2,
            margin: 20
        },
        480: {
            items: 3,
            margin: 30
        },
        576: {
            items: 4,
            margin: 40
        },
        767: {
            items: 4
        },
        991: {
            items: 5
        },
        1000: {
            items: 5
        }
    }
});

/*************************
 brand js end
 *************************/
/*************************
 brand js start
 *************************/

$('#wedding-slider').owlCarousel({
    items: 5,
    margin: 80,
    autoHeight: true,
    nav: false,
    autoplay: true,
    slideSpeed: 300,
    paginationSpeed: 400,
    loop: true,
    responsive: {
        0: {
            items: 1,
            margin: 10
        },
        320: {
            items: 1,
            margin: 10
        },
        360: {
            items: 2,
            margin: 10
        },
        480: {
            items: 3,
            margin: 15
        },
        576: {
            items: 4,
            margin: 15
        },
        767: {
            items: 4
        },
        991: {
            items: 5
        },
        1000: {
            items: 5
        }
    }
});

/*************************
 brand js end
 *************************/
