(function($) {
    "use strict";

    /*************************
     testimonial js start
     *************************/

    $('#music-testimonial-slider').owlCarousel({
        items: 1,
        margin: 0,
        dots: false,
        nav: true,
        navText: ['<i class="fa fa-chevron-left" aria-hidden="true"></i>', '<i class="fa fa-chevron-right" aria-hidden="true"></i>'],
        autoplay: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true
    });

    /*************************
     testimonial js end
     *************************/

    /*************************
     music-slider js start
     *************************/

    $('#music-slider').owlCarousel({
        items: 1,
        margin: 0,
        autoHeight: true,
        nav: false,
        autoplay: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true,
    });

    /*************************
     music-slider js end
     *************************/

    /*************************
     gallery-slider js start
     *************************/

    $('#gallery-slider').owlCarousel({
        items: 5,
        autoHeight: true,
        nav: true,
        navText: ['<img src="../assets/images/music/gallery/gallery-icon/left.png">', '<img src="../assets/images/music/gallery/gallery-icon/right.png">'],
        autoplay: false,
        center: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        dots: false,
        loop: true,
        responsive: {
            0: {
                items: 1,
                margin: 10
            },
            460: {
                items: 2
            },
            480: {
                items: 3
            },
            767: {
                items: 4
            },
            991: {
                items: 5
            }
        }
    });

    /*************************
     gallery-slider js end
     *************************/

    /*************************
     blog js start
     *************************/

    $('#blog-slider').owlCarousel({
        items: 3,
        margin: 60,
        nav: false,
        dots:false,
        autoplay: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            575: {
                items: 2,
                margin: 10
            },
            991: {
                items: 3,
                margin: 10
            },
            1600: {
                margin: 30
            }
        }
    });

    /*************************
     blog js end
     *************************/
    
    /*************************
     blog js start
     *************************/

    $('.artist-slider').owlCarousel({
        items: 3,
        margin: 60,
        nav: false,
        dots:false,
        autoplay: false,
        slideSpeed: 300,
        paginationSpeed: 400,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            767: {
                items: 2
            },
            991: {
                items: 3,
                margin: 20
            },
            1600: {
                margin: 30
            }
        }
    });

    /*************************
     blog js end
     *************************/


    function openNav() {
        document.getElementById("mySidenav").classList.add('open-side');
    }
    function closeNav() {
        document.getElementById("mySidenav").classList.remove('open-side');
    }

})(jQuery);
