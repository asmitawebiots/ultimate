/*************************
 counter js start
 *************************/

"use strict";
$(document).ready(function () {
    $('.counts').counterUp({
        delay: 10,
        time: 1000
    });
});

/*************************
 counter js end
 *************************/