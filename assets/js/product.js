$('.product-slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-nav'
});

$('.slider-nav').slick({
    vertical: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-slick',
    arrows: false,
    dots: false,
    focusOnSelect: true
});

$('.product-right-slick').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    asNavFor: '.slider-right-nav'
});
if ($(window).width() > 575) {
    $('.slider-right-nav').slick({
        vertical: true,
        verticalSwiping: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-right-slick',
        arrows: false,
        infinite: true,
        dots: false,
        centerMode: false,
        focusOnSelect: true
    });
}else{
    $('.slider-right-nav').slick({
        vertical: false,
        verticalSwiping: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.product-right-slick',
        arrows: false,
        infinite: true,
        centerMode: false,
        dots: false,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            }
        ]
    });
}
$(document).ready(function() {
    if ($(window).width() > 991) {
        $(".product_img_scroll, .pro_sticky_info").stick_in_parent();
    }
});


/*=====================
 07. Quantity Counter
 ==========================*/
$('.quantity-right-plus').on('click', function () {
    var $qty = $('.qty-box .input-number');
    var currentVal = parseInt($qty.val(), 10);
    if (!isNaN(currentVal)) {
        $qty.val(currentVal + 1);
    }
});
$('.quantity-left-minus').on('click', function () {
    var $qty = $('.qty-box .input-number');
    var currentVal = parseInt($qty.val(), 10);
    if (!isNaN(currentVal) && currentVal > 1) {
        $qty.val(currentVal - 1);
    }
});

