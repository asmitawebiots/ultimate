(function ($) {
    "use strict";

    /*************************
     scroll down start
     *************************/
    $(function() {
        $('.down').click (function() {
            $('html, body').animate({scrollTop: $('.booking').offset().top }, 'slow');
            return false;
        });
    });

    /*************************
     scroll down end
     *************************/    
    /*************************
     loader js start
     *************************/

    $('.loader-wrapper').fadeOut('slow', function () {
        $(this).remove();
    });

    /*************************
     loader js end
     *************************/
    /*************************
     add class on scroll js start
     *************************/

    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 800) {
            $("header").addClass("header-fixed").removeClass("header-absolute");
        } else {
            $("header").removeClass("header-fixed").addClass("header-absolute");
        }
    });
    if ($(window).width() <= 991) {
        $('.menu-container').find('.inner-collapse').addClass("collapse");
        $(".dropdown-sec").on('click', function () {
            $('.navbar-nav').find('.dropdown-content').removeClass("demo");
            $(this).find('.dropdown-content').addClass("demo");
        });

        $(".menu-container").on('click', function () {
            $('.menu-container').find('.inner-collapse').removeClass("demo2");
            $(this).find('.inner-collapse').removeClass("collapse");
            $(this).find('.inner-collapse').addClass("demo2");
        });

        //demo2
    }

    /*************************
     add class on scroll js end
     *************************/


    /*************************
     tap to top js start
     *************************/

    $(window).on('scroll', function () {
        if ($(this).scrollTop() > 500) {
            $('.tap-top').fadeIn();
        } else {
            $('.tap-top').fadeOut();
        }
    });
    $('.tap-top').on('click', function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    /*************************
     tap to top js end
     *************************/


    /*************************
     scroll js start
     *************************/

    "use strict";
    if ($(window).width() >= 991) {
        $("nav a").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 45
                }, 1000, function () {
                });
                return false;
            }
        });
    } else {
        $("nav a").on('click', function (event) {
            if (this.hash !== "") {
                event.preventDefault();
                var hash = this.hash;
                $('html, body').animate({
                    scrollTop: $(hash).offset().top - 50
                }, 1000, function () {
                });
                return false;
            }
        });
    }

    /*************************
     scroll js end
     *************************/

    /*************************
     Footer title Start
     *************************/
    var contentwidth = jQuery(window).width();
    if ((contentwidth) < '768') {
        jQuery('.footer-title h3').append('<span class="according-menu"></span>');
        jQuery('.footer-title').on('click', function () {
            jQuery('.footer-title').removeClass('active');
            jQuery('.footer-contant').slideUp('normal');
            if (jQuery(this).next().is(':hidden') == true) {
                jQuery(this).addClass('active');
                jQuery(this).next().slideDown('normal');
            }
        });
        jQuery('.footer-contant').hide();
    } else {
        jQuery('.footer-contant').show();
    }

    /*************************
     Footer title End
     *************************/
   
})(jQuery);